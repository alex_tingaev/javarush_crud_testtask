package crud.vaadin;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import crud.backend.User;
import crud.backend.UserRepository;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventScope;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.components.DisclosurePanel;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.label.RichText;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

@Title("CRUD TestTask for JR with love")
@Theme("valo")
@SpringUI
public class MainUI extends UI {

    private static final long serialVersionUID = 1L;
    
    private UserRepository repo;
    private UserForm userForm;
    private EventBus.UIEventBus eventBus;
    
    private MTable<User> list = new MTable<>(User.class)
            .withProperties("id", "name", "age", "isadmin", "createddate")
            .withColumnHeaders("Номер", "Имя", "Возраст", "Права администратора", "Дата создания записи")
            .setSortableProperties("id", "name", "age", "isadmin", "createddate")
            .withFullWidth();
    
    private TextField filterByName = new MTextField()
            .withInputPrompt("Поиск по имени");
    private Button addNew = new MButton(FontAwesome.PLUS, this::add);
    private Button edit = new MButton(FontAwesome.PENCIL_SQUARE_O, this::edit);
    private Button delete = new ConfirmButton(FontAwesome.TRASH_O,
            "Действительно удалить запись, насовсем?", this::remove);

    public MainUI(UserRepository r, UserForm f, EventBus.UIEventBus b) {
        this.repo = r;
        this.userForm = f;
        this.eventBus = b;
    }
    
    @Override
    protected void init(VaadinRequest request) {
        DisclosurePanel aboutBox = new DisclosurePanel("Небольшой дисклеймер", new RichText().withMarkDownResource("/welcome.md"));
        setContent(
                new MVerticalLayout(
                        aboutBox,
                        new MHorizontalLayout(filterByName, addNew, edit, delete),
                        list
                ).expand(list)
        );
        listEntities();
        
        list.addMValueChangeListener(e -> adjustActionButtonState());
        filterByName.addTextChangeListener(e -> listEntities(e.getText()));

        // Listen to change events emitted by PersonForm see onEvent method
        eventBus.subscribe(this);
    }
    
    private void adjustActionButtonState() {
        boolean hasSelection = list.getValue() != null;
        edit.setEnabled(hasSelection);
        delete.setEnabled(hasSelection);
    }
    

    
    private void listEntities() {
        listEntities(filterByName.getValue());
    }
    
    private void listEntities(String nameFilter) {

        String likeFilter = "%" + nameFilter + "%";
        list.setRows(repo.findByNameLikeIgnoreCase(likeFilter));

        adjustActionButtonState();
    }
    
    private void add(ClickEvent clickEvent) {
        edit(new User());
    }
    
    private void edit(ClickEvent e) {
        edit(list.getValue());
    }
    
    private void remove(ClickEvent e) {
        repo.delete(list.getValue());
        list.setValue(null);
        listEntities();
    }
    
    private void edit(final User phoneBookEntry) {
        userForm.setEntity(phoneBookEntry);
        userForm.openInModalPopup();
    }

    @EventBusListenerMethod(scope = EventScope.UI)
    public void onPersonModified(UserModifiedEvent event) {
        listEntities();
        userForm.closePopup();
    }
    
}
