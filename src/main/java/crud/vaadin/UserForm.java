package crud.vaadin;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import crud.backend.User;
import crud.backend.UserRepository;
import org.vaadin.spring.events.EventBus;
import org.vaadin.teemu.switchui.Switch;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

@UIScope
@SpringComponent
public class UserForm extends AbstractForm<User> {

    private static final long serialVersionUID = 1L;

    private EventBus.UIEventBus eventBus;
    private UserRepository repo;

    private TextField name = new MTextField("Имя");
    private TextField age = new MTextField("Возраст");
    private Switch isadmin = new Switch("Права Администратора");
    private DateField createddate = new DateField("Дата создания записи");


    UserForm(UserRepository r, EventBus.UIEventBus b) {
        this.repo = r;
        this.eventBus = b;


        setSavedHandler(user -> {

            repo.save(user);
            eventBus.publish(this, new UserModifiedEvent(user));
        });
        setResetHandler(p -> eventBus.publish(this, new UserModifiedEvent(p)));

        setSizeUndefined();
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                new MFormLayout(
                        name,
                        age,
                        isadmin,
                        createddate
                ).withWidth(""),
                getToolbar()
        ).withWidth("");
    }

}
