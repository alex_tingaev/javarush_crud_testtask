package crud.vaadin;

import crud.backend.User;

import java.io.Serializable;

public class UserModifiedEvent implements Serializable {

    private final User user;

    UserModifiedEvent(User user) {
        this.user = user;
    }

    public User getUserModifiedEvent() {
        return user;
    }
    
}
