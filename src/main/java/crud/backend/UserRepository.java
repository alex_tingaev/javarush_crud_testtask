
package crud.backend;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Empty JpaRepository is enough for a simple crud.
 */
public interface UserRepository extends JpaRepository<User, Integer> {
    
    /* A version to fetch List instead of Page to avoid extra count query. */
    List<User> findAllBy(Pageable pageable);
    
    List<User> findByNameLikeIgnoreCase(String nameFilter);
    
    // For lazy loading and filtering
    List<User> findByNameLikeIgnoreCase(String nameFilter, Pageable pageable);
    
    long countByNameLike(String nameFilter);

}
