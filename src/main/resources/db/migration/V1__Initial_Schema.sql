DROP TABLE IF EXISTS User;
CREATE TABLE User (
  id INT AUTO_INCREMENT,
  name VARCHAR(25) NOT NULL,
  age INT,
  isadmin BIT DEFAULT 0,
  createddate TIMESTAMP,

  PRIMARY KEY (id));

insert into User (id, name, age, createddate) values (1, 'Donna', 1, NOW());
insert into User (id, name, age, createddate) values (2, 'Tammy', 2, NOW());
insert into User (id, name, age, createddate) values (3, 'Daniel', 3, NOW());
insert into User (id, name, age, createddate) values (4, 'Jacqueline', 4, NOW());
insert into User (id, name, age, createddate) values (5, 'Ronald', 5, NOW());
insert into User (id, name, age, createddate) values (6, 'Rachel', 6, NOW());
insert into User (id, name, age, createddate) values (7, 'Jessica', 7, NOW());
insert into User (id, name, age, createddate) values (8, 'Tammy', 8, NOW());
insert into User (id, name, age, createddate) values (9, 'Amy', 9, NOW());
insert into User (id, name, age, createddate) values (10, 'Donna', 10, NOW());
insert into User (id, name, age, createddate) values (11, 'Annie', 11, NOW());
insert into User (id, name, age, createddate) values (12, 'Craig', 12, NOW());
insert into User (id, name, age, createddate) values (13, 'Juan', 13, NOW());
insert into User (id, name, age, createddate) values (14, 'Michael', 14, NOW());
insert into User (id, name, age, createddate) values (15, 'Marilyn', 15, NOW());
insert into User (id, name, age, createddate) values (16, 'Peter', 16, NOW());
insert into User (id, name, age, createddate) values (17, 'Anthony', 17, NOW());
insert into User (id, name, age, createddate) values (18, 'Daniel', 18, NOW());
insert into User (id, name, age, createddate) values (19, 'John', 19, NOW());
insert into User (id, name, age, createddate) values (20, 'Marilyn', 20, NOW());
insert into User (id, name, age, createddate) values (21, 'Jessica', 21, NOW());
insert into User (id, name, age, createddate) values (22, 'Irene', 22, NOW());
insert into User (id, name, age, createddate) values (23, 'Robert', 23, NOW());
insert into User (id, name, age, createddate) values (24, 'Denise', 24, NOW());
insert into User (id, name, age, createddate) values (25, 'Donna', 25, NOW());
insert into User (id, name, age, createddate) values (26, 'Rebecca', 26, NOW());
insert into User (id, name, age, createddate) values (27, 'Frank', 27, NOW());
insert into User (id, name, age, createddate) values (28, 'Thomas', 28, NOW());
insert into User (id, name, age, createddate) values (29, 'Gerald', 29, NOW());
insert into User (id, name, age, createddate) values (30, 'Shawn', 30, NOW());
insert into User (id, name, age, createddate) values (31, 'Sara', 31, NOW());
insert into User (id, name, age, createddate) values (32, 'Robert', 32, NOW());
insert into User (id, name, age, createddate) values (33, 'Rebecca', 33, NOW());
insert into User (id, name, age, createddate) values (34, 'Anthony', 34, NOW());
insert into User (id, name, age, createddate) values (35, 'Jean', 35, NOW());
insert into User (id, name, age, createddate) values (36, 'John', 36, NOW());
insert into User (id, name, age, createddate) values (37, 'William', 37, NOW());
insert into User (id, name, age, createddate) values (38, 'Pamela', 38, NOW());
insert into User (id, name, age, createddate) values (39, 'Cynthia', 39, NOW());
insert into User (id, name, age, createddate) values (40, 'Alan', 40, NOW());
insert into User (id, name, age, createddate) values (41, 'Harry', 41, NOW());
insert into User (id, name, age, createddate) values (42, 'Marilyn', 42, NOW());
insert into User (id, name, age, createddate) values (43, 'Teresa', 43, NOW());
insert into User (id, name, age, createddate) values (44, 'Robert', 44, NOW());
insert into User (id, name, age, createddate) values (45, 'Todd', 45, NOW());
insert into User (id, name, age, createddate) values (46, 'Johnny', 46, NOW());
insert into User (id, name, age, createddate) values (47, 'Terry', 47, NOW());
insert into User (id, name, age, createddate) values (48, 'Jane', 48, NOW());
insert into User (id, name, age, createddate) values (49, 'Richard', 49, NOW());
insert into User (id, name, age, createddate) values (50, 'Donna', 50, NOW());