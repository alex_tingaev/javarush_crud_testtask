# Spring Data JPA CRUD with Vaadin

Заводится любым удобным способом - 

1. импорт в ИДЕ и пуск main
2. mvn spring-boot:run
3. mvn package, потом java -jar target/приложеньице.jar
4. Попробовать задеплоить вот так:
    mvn install
    cf push ваш-любимый-сервер -p target/*.jar -b https://github.com/cloudfoundry/java-buildpack.git
    
/src/main/resources/db/migration/V1__Initial_Schema.sql - скрипт для создания и наполнения базы, все автоматически, по идее, ничего
    отдельно запускать не придется. 
    
настройки DB в application-local.properties (как и завещано, test root root). 

Если вдруг случилось чудо и всё завелось, посмотреть можно на http://localhost:8080/